﻿using UnityEngine;
using System.Collections;

public class PlayerStats {

	public int			Score			{ get; set; }
	public int			Experience		{ get; set; }
	public int			Health			{ get; set; }
	public int			Power			{ get; set; }
	public int			Coins			{ get; set; }
		
}
