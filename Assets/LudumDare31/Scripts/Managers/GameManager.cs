﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public int			NumberPlayers;
	public GameObject	scene;
	
	private Character	playerOne;
	private Character	playerTwo;
	
	private GameObject	p1;
	private GameObject	p2;
	
	private string		p2Controller;
	

	public void SetP2(string controller) {
		p2Controller = controller;
		playerTwo.Controller = controller;
	}
	
	public void EndGame(int player) {
		GameController.Instance.ScoreManager.IsActive = false;
		if (player == 2)
			playerOne.GetController().EndGame();
		
		if (player == 1)
			playerTwo.GetController().EndGame();

		StartCoroutine(endGame(player));
	}	
	
	public void ResetGame() {
		GameController.Instance.WallpaperManager.ShowWallpaper();
		Destroy(p1);
		Destroy(p2);
	}

	public void InitGame1() {
		
		p1 = GameController.Instance.CharacterManager.CreateCharacter("Dummy");
		p1.transform.parent = scene.transform;
		p1.transform.localPosition = new Vector2(-3,0);
		SetPlayerOne("Dummy", "Keyboard1CharacterController");
		
		p2 = GameController.Instance.CharacterManager.CreateCharacter("Dummy");
		p2.transform.parent = scene.transform;
		p2.transform.localPosition = new Vector2(3,0);
		p2Controller = "DumbCharacterController";
		SetPlayerTwo("Dummy", p2Controller);
				
		GameController.Instance.ScoreManager.PlayerOne.Health = playerOne.Health;
		GameController.Instance.ScoreManager.PlayerTwo.Health = playerTwo.Health;
		
		GameController.Instance.GuiManager.ShowDifficulty();
		
	}
	
	public void InitGame2() {
	
		p1 = GameController.Instance.CharacterManager.CreateCharacter("Dummy");
		p1.transform.parent = scene.transform;
		p1.transform.localPosition = new Vector2(-3,0);
		SetPlayerOne("Dummy", "Keyboard1CharacterController");
		
		p2 = GameController.Instance.CharacterManager.CreateCharacter("Dummy");
		p2.transform.parent = scene.transform;
		p2.transform.localPosition = new Vector2(3,0);
		p2Controller = "Keyboard2CharacterController";
		SetPlayerTwo("Dummy", p2Controller);
		
		GameController.Instance.ScoreManager.PlayerOne.Health = playerOne.Health;
		GameController.Instance.ScoreManager.PlayerTwo.Health = playerTwo.Health;
	}
	
	public void SetPlayerOne(string character, string controller) {
		playerOne = p1.GetComponent<Character>();
		Character c = GameController.Instance.CharacterManager.GetCharacter(character);
		playerOne.Name = c.Name;
		playerOne.Skin = c.Skin;
		playerOne.Strength = c.Strength;
		playerOne.Stamina = c.Stamina;
		playerOne.Aggression = c.Aggression;
		playerOne.Recovery = c.Recovery;
		playerOne.Defense = c.Defense;
		playerOne.Prefab = c.Prefab;
		playerOne.Skin = c.Skin;
		playerOne.Size = c.Size;
		playerOne.Speed = c.Speed;
		playerOne.HoldLeft = c.HoldLeft;
		playerOne.HoldRight = c.HoldRight;
		playerOne.Controller = c.Controller;
		playerOne.Hat = c.Hat;
		playerOne.Health = c.Health;
		playerOne.Player = "Player1";
		playerOne.Enemy = "Player2";
		playerOne.Controller = controller;
		GameController.Instance.ScoreManager.PlayerOne.Health = c.Health;
		playerOne.Init();
	}
	
	public void SetPlayerTwo(string character, string controller) {
		playerTwo = p2.GetComponent<Character>();
		Character c = GameController.Instance.CharacterManager.GetCharacter(character);
		playerTwo.Name = c.Name;
		playerTwo.Skin = c.Skin;
		playerTwo.Strength = c.Strength;
		playerTwo.Stamina = c.Stamina;
		playerTwo.Aggression = c.Aggression;
		playerTwo.Recovery = c.Recovery;
		playerTwo.Defense = c.Defense;
		playerTwo.Prefab = c.Prefab;
		playerTwo.Skin = c.Skin;
		playerTwo.Size = c.Size;
		playerTwo.Speed = c.Speed;
		playerTwo.HoldLeft = c.HoldLeft;
		playerTwo.HoldRight = c.HoldRight;
		playerTwo.Controller = c.Controller;
		playerTwo.Hat = c.Hat;
		playerTwo.Health = c.Health;
		playerTwo.Player = "Player2";
		playerTwo.Enemy = "Player1";
		playerTwo.Controller = p2Controller;
		GameController.Instance.ScoreManager.PlayerTwo.Health = c.Health;
		playerTwo.Init();
	}
	
	public void StartGame() {
		GameController.Instance.ScoreManager.IsActive = true;
		StartCoroutine(startGame());
	}

	void OnEnable() {
		GameController.Instance.GameManager = this;
		scene = GameObject.Find("Scene");
	}
	
	private IEnumerator startGame() {
	
		GameController.Instance.GuiManager.CountdownText.text = "";
		
		GameController.Instance.GuiManager.VoiceText.text = playerOne.Name;
		GameController.Instance.VoiceManager.PlayCharacter(playerOne.Name);
		yield return new WaitForSeconds(1.5f);
		
		GameController.Instance.GuiManager.VoiceText.text = "vs.";
		GameController.Instance.VoiceManager.PlayVersus();
		yield return new WaitForSeconds(1.5f);
		
		GameController.Instance.GuiManager.VoiceText.text = playerTwo.Name;
		GameController.Instance.VoiceManager.PlayCharacter(playerTwo.Name);
		yield return new WaitForSeconds(2.0f);
		
		GameController.Instance.GuiManager.VoiceText.text = "";
		
		GameController.Instance.VoiceManager.PlayFight();
		yield return new WaitForSeconds(0.5f);
		GameController.Instance.GuiManager.CountdownText.text = "3";
		yield return new WaitForSeconds(1.0f);
		GameController.Instance.GuiManager.CountdownText.text = "2";
		yield return new WaitForSeconds(1.0f);
		GameController.Instance.GuiManager.CountdownText.text = "1";
		yield return new WaitForSeconds(1.0f);
		GameController.Instance.GuiManager.CountdownText.text = "FIGHT!";
		playerOne.StartGame();
		playerTwo.StartGame();
		yield return new WaitForSeconds(2.0f);
		GameController.Instance.GuiManager.CountdownText.text = "";
		yield return null;
	}
	
	private IEnumerator endGame(int winner) {
	
		GameController.Instance.GuiManager.CountdownText.text = "";
		
		GameController.Instance.GuiManager.VoiceText.text = "winner";
		GameController.Instance.VoiceManager.PlayWinner();
		yield return new WaitForSeconds(1.5f);
		
		if (winner == 1) {		
			GameController.Instance.GuiManager.VoiceText.text = "Player 1";
			GameController.Instance.VoiceManager.PlayPlayerOne();
		} else {
			GameController.Instance.GuiManager.VoiceText.text = "Player 2";
			GameController.Instance.VoiceManager.PlayPlayerTwo();
		}
		
		yield return new WaitForSeconds(1.5f);
		
		GameController.Instance.GuiManager.RoundOver();
		
		yield return null;
		
	}
}
