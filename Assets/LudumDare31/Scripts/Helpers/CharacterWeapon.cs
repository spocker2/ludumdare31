﻿using UnityEngine;
using System.Collections;

public class CharacterWeapon : MonoBehaviour {

	private int			enemy;
	private int			player;
	
	private Character	character;
	
	private bool		blood;
	
	void OnCollisionEnter2D(Collision2D collision) {
		D.Trace("[CharacterWeapon] OnCollisionEnter");
		D.Log("collided with {0} {1}", collision.gameObject.tag, collision.gameObject);
		
		if (character == null)
			return;
		
		
		if (collision.gameObject.tag == character.Enemy) {
			
			float speed = collision.relativeVelocity.magnitude;
			D.Detail("weapon speed {0}", speed);
			
			if (character.GetController().GetState() != CharacterStates.ATTACKING)
				return;
				
			if (speed < 20)
				return;
			
			if (!blood) {
				Transform location = collision.transform;
				GameObject e = GameController.Instance.EffectManager.LongBloodEffect();
				e.transform.parent = location;
				e.transform.localPosition = new Vector2(0,0);
				StartCoroutine(bloodDelay());
			}
								
			GameController.Instance.ScoreManager.AddHealth(enemy, -(30+character.Strength+(int)speed));
			GameController.Instance.ScoreManager.AddScore(player, 300);
			GameController.Instance.ScoreManager.AddExperience(player, 150);
			GameController.Instance.ScoreManager.AddPower(player, 75);
			
			GameController.Instance.SoundManager.PlayHit();
			
			if (collision.gameObject.name == "Head") {
				GameController.Instance.VoiceManager.PlayHeadshot();
				collision.gameObject.rigidbody2D.AddForce(new Vector2(0,character.Strength), ForceMode2D.Impulse);
			}
		}
	}
	
	public void StartGame() {
		character = transform.parent.parent.parent.GetComponent<Character>();
		enemy = 1;
		player = 2;
		if (character.Enemy == "Player2") {
			player = 1;
			enemy = 2;
		}
	}
	
	private IEnumerator bloodDelay() {
		blood = true;
		yield return new WaitForSeconds(1.0f);
		blood = false;
		yield return null;
	}
	
	void OnEnable() {
		D.Trace("[CharacterWeapon] OnEnable");
	}
	
	
}
