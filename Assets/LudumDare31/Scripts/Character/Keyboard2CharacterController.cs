﻿using UnityEngine;
using System.Collections;

public class Keyboard2CharacterController : CharacterController {

	void Update () {
		
		if (Input.GetKeyDown(KeyCode.RightArrow)) {
			AttackRight();
		}	
		
		if (Input.GetKeyDown(KeyCode.LeftArrow)) {
			AttackLeft();
		}	
		
		if (Input.GetKeyDown(KeyCode.UpArrow)) {
			Jump();
		}	
		
	}
	
	
}
