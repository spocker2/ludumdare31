﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

	//	PUBLIC
	
	//	PRIVATE
	
	//	MONO
	
	public AudioClip		TitleMusic;
	public AudioClip[]		GameMusic;
	
	public void PlayTitleMusic() {
		D.Trace("[GameMusic] PlayTitleMusic");
		__playMusic(TitleMusic);
	}
	
	public void PlayGameMusic() {
		D.Trace("[GameMusic] PlayGameMusic");
		__playMusic(GameMusic[Random.Range(0,GameMusic.Length)]);	
	}
		
	private void __playMusic(AudioClip clip) {
		
		D.Trace("[GameMusic] __playMusic");
		
		if (audio.clip != clip) {
			audio.Stop();
			audio.clip = clip;
			audio.Play();
		}
	}
	
	void OnEnable() {
		D.Trace("[MusicManager] OnEnable");
		GameController.Instance.MusicManager = this;
	}
	
	void Start() {
		D.Trace("[GameMusic] Start");
	}
	
}
