﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A dumb controller that does dumb random things
/// </summary>
public class DumbestCharacterController : CharacterController {

	//	PUBLIC
	
	public override void OnStart ()
	{
		base.OnStart ();
		StartCoroutine(randomThings());
		
	}
	
	//	PRIVATE
	
	private IEnumerator randomThings() {
		while(true) {
		
			yield return new WaitForSeconds(0.75f);
			
			int r = Random.Range(0,100);
			D.Log(r);
			
			if (r < 50) {
				Idle();
				continue;
			}
			
			if (r < 60) {
				Jump();
				continue;
			}
				
			if (r < 80) {
				AttackLeft();
				continue;
			}
			
			if (r < 100) {
				AttackRight();
				continue;
			}
		}
	}
	
	//	MONO
	
}
