﻿using UnityEngine;
using System.Collections;

public class SelectPlayer1 : MonoBehaviour {

	public UnityEngine.UI.Text	CharacterName;
	public UnityEngine.UI.Text	Selector;
	public GameObject			P1Dummy;
	public GameObject			P1Snoman;
	public GameObject			P1Ninja;
	public GameObject			P1Human;
	public GameObject			P1Nutcracker;
	public GameObject			P1Giant;
	public GameObject			P1Troll;
	public GameObject			P1Soccer;
	
	
	public void SelectDummy() {
		Select("Dummy", P1Dummy);
	}
	
	public void SelectSnowman() {
		Select("Snowman", P1Snoman);
	}
	
	public void SelectNinja() {
		Select("Ninja", P1Ninja);
	}
	
	public void SelectHuman() {
		Select("Human", P1Human);
	}
	
	public void SelectNutcracker() {
		Select("Nutcracker", P1Nutcracker);
	}
	
	public void SelectGiant() {
		Select("Chuck", P1Giant);
	}
	
	public void SelectTroll() {
		Select("Troll", P1Troll);
	}
	
	public void SelectSoccer() {
		Select("Soccer", P1Soccer);
	}
	
	private void Select(string character, GameObject go) {
		while(true) { }
		GameController.Instance.GameManager.SetPlayerOne(character, "Keyboard1CharacterController");
		CharacterName.text = character;
		Selector.transform.position = go.transform.position;
	}
}
