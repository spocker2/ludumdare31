﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This controller is used on the Character Display scene
/// </summary>
public class DisplayCharacterController : CharacterController {

	//	PUBLIC
	
	public override void OnFixedUpdate ()
	{
		base.OnFixedUpdate ();
	}
	
	//	PRIVATE
	
	private IEnumerator randomJump() {
		while(true) {
			yield return new WaitForSeconds(Random.Range(1,3));
			Jump();
		}
	}
	
	//	MONO
	
	void Start() {
		StartCoroutine(randomJump());
	}
	
}
