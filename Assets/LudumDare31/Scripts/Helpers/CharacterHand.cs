﻿using UnityEngine;
using System.Collections;

public class CharacterHand : MonoBehaviour {

	private int			enemy;
	private int			player;
	private Character	character;
	
	private bool		blood;
	
	void OnCollisionEnter2D(Collision2D collision) {
	
		D.Trace("[CharacterHand] OnCollisionEnter");
		D.Log("collided with {0} {1}", collision.gameObject.tag, collision.gameObject);
		
		if (character == null)
			return;
		
		if (collision.gameObject.tag == character.Enemy) {
		
			float speed = collision.relativeVelocity.magnitude;
			D.Detail("hand speed {0}", speed);
			
			if (speed < 15)
				return;
				
			if (character.GetController().GetState() != CharacterStates.ATTACKING)
				return;
			
			if (!blood) {
				Transform location = collision.transform;
				GameObject e = GameController.Instance.EffectManager.ShortBloodEffect();
				e.transform.parent = location;
				e.transform.localPosition = new Vector2(0,0);
				StartCoroutine(bloodDelay());
			}
		
			GameController.Instance.ScoreManager.AddHealth(enemy, -(10+character.Strength+(int)speed));
			GameController.Instance.ScoreManager.AddScore(player, 100);
			GameController.Instance.ScoreManager.AddExperience(player, 50);
			GameController.Instance.ScoreManager.AddPower(player, 25);
			
			GameController.Instance.SoundManager.PlayHit();
		}
	}
	
	public void StartGame() {
		character = transform.parent.parent.GetComponent<Character>();
		enemy = 1;
		player = 2;
		if (character.Enemy == "Player2") {
			player = 1;
			enemy = 2;
		}
	}
	
	private IEnumerator bloodDelay() {
		blood = true;
		yield return new WaitForSeconds(1.0f);
		blood = false;
		yield return null;
	}
	
	void OnEnable() {
	}

}
