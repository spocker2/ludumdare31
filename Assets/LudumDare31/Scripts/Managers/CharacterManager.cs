﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterManager : MonoBehaviour {

	public GameObject			PlayerPrefab;
	
	private Dictionary<string, Character>	characters;
	
	public GameObject CreateCharacter(string name) {
		
		GameObject go = (GameObject)Instantiate(PlayerPrefab);
		Character gc = go.transform.GetComponent<Character>();
		Character c = characters[name];
		gc.Name = c.Name;
		gc.Skin = c.Skin;
		gc.Strength = c.Strength;
		gc.Stamina = c.Stamina;
		gc.Aggression = c.Aggression;
		gc.Recovery = c.Recovery;
		gc.Defense = c.Defense;
		gc.Prefab = c.Prefab;
		gc.Skin = c.Skin;
		gc.Size = c.Size;
		gc.Speed = c.Speed;
		gc.HoldLeft = c.HoldLeft;
		gc.HoldRight = c.HoldRight;
		gc.Controller = c.Controller;
		gc.Hat = c.Hat;
		gc.Health = c.Health;
		return go;
		
	}
	
	public Character GetCharacter(string name) {
		return characters[name];
	}
	
	private void loadCharacters() {
	
		characters = new Dictionary<string, Character>();
		
		Character c;
		
		c = new Character();
		c.Name = "Dummy";
		c.Strength = 20;
		c.Speed = 10;
		c.Stamina = 10;
		c.Health = 4000;
		c.Skin = "dummy";
		c.Hat = "";
		c.Recovery = 30;
		characters.Add(c.Name, c);
		
		c = new Character();
		c.Name = "Snowman";
		c.Strength = 25;
		c.Speed = 10;
		c.Stamina = 100;
		c.Health = 6000;
		c.Skin = "snowman";
		c.Hat = "top";
		c.Recovery = 20;
		characters.Add(c.Name, c);
		
		c = new Character();
		c.Name = "Human";
		c.Strength = 20;
		c.Speed = 15;
		c.Stamina = 25;
		c.Health = 4000;
		c.Skin = "human";
		c.Hat = "baseball";
		c.Recovery = 5;
		characters.Add(c.Name, c);
		
		c = new Character();
		c.Name = "Ninja";
		c.Strength = 15;
		c.Speed = 30;
		c.Stamina = 55;
		c.Health = 3000;
		c.Skin = "ninja";
		c.Hat = "";
		c.Recovery = 15;
		characters.Add(c.Name, c);
		
		c = new Character();
		c.Name = "Nutcracker";
		c.Strength = 35;
		c.Speed = 5;
		c.Stamina = 40;
		c.Health = 5000;
		c.Skin = "nutcracker";
		c.Hat = "crown";
		c.Recovery = 15;
		characters.Add(c.Name, c);
		
		c = new Character();
		c.Name = "Chuck";
		c.Strength = 35;
		c.Speed = 25;
		c.Stamina = 35;
		c.Health = 12000;
		c.Skin = "chuck";
		c.Hat = "chuck";
		c.Recovery = 15;
		c.Size = 1;
		characters.Add(c.Name, c);
		
		c = new Character();
		c.Name = "Troll";
		c.Strength = 15;
		c.Speed = 45;
		c.Stamina = 15;
		c.Health = 7000;
		c.Skin = "troll";
		c.Hat = "";
		c.Recovery = 5;
		characters.Add(c.Name, c);
		
		c = new Character();
		c.Name = "Soccer";
		c.Strength = 25;
		c.Speed = 35;
		c.Stamina = 25;
		c.Health = 6000;
		c.Skin = "nutcracker";
		c.Hat = "";
		c.Recovery = 15;
		characters.Add(c.Name, c);
		
	}
	
	void OnEnable() {
		GameController.Instance.CharacterManager = this;
		loadCharacters();
	}

}
