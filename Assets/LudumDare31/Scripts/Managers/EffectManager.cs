﻿using UnityEngine;
using System.Collections;

public class EffectManager : MonoBehaviour {

	public GameObject		EffectBlood;
	public GameObject		EffectRedBlood;
	
	private GameObject		blood;
	
	//	PUBLIC
	
	public GameObject ShortBloodEffect() {
		return createEffect(blood, 0.5f);
	}
	
	public GameObject MediumBloodEffect() {
		return createEffect(blood, 2.0f);
	}
	
	public GameObject LongBloodEffect() {
		return createEffect(blood, 5.0f);
	}
	
	//	PRIVATE
	
	private GameObject createEffect(GameObject effect, float life) {
		D.Trace("[EffectManager] createEffect");
		GameObject go = (GameObject)Instantiate(effect);
		Destroy(go, life);
		return go;
	}
	
	//	MONO
	
	void Start() {
	
		if (GameController.Instance.GameSettings.GameBlood) {
			blood = EffectRedBlood;
		} else {
			blood = EffectBlood;
		}
	}
	
	void OnEnable() {
		D.Trace("[EffectManager] OnEnable");
		GameController.Instance.EffectManager = this;
		
	}
}
