﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {
	
	public PlayerStats		PlayerOne	{ get; set; }
	public PlayerStats		PlayerTwo	{ get; set; }
	
	public  bool			IsActive;
	
	private bool			p1Finish;
	private bool			p2Finish;

	public void AddScore(int player, int score) {
		
		if (!IsActive) return;
		
		if (player == 1) {
			PlayerOne.Score += score;
		} else {
			PlayerTwo.Score += score;
		}
	}
	
	public void AddExperience(int player, int xp) {
		
		if (!IsActive) return;
		if (player == 1) {
			PlayerOne.Experience += xp;
		} else {
			PlayerTwo.Experience += xp;
		}
	}
	
	public void AddHealth(int player, int health) {
		
		if (!IsActive) return;
		
		D.Warn(health);
		
		if (player == 1) {
			PlayerOne.Health += health;
			if (PlayerOne.Health < 1000 && !p1Finish) {
				p1Finish = true;
				StartCoroutine(finish());
			}
			if (PlayerOne.Health <= 0)
				GameController.Instance.GameManager.EndGame(2);
		} else {
			PlayerTwo.Health += health;
			if (PlayerTwo.Health < 1000 && !p2Finish) {
				p2Finish = true;
				StartCoroutine(finish());
			}
			if (PlayerTwo.Health <= 0)
				GameController.Instance.GameManager.EndGame(1);
		}
		
		if (health < -100) {
			GameController.Instance.VoiceManager.PlaySuperb();
			StartCoroutine(voiceText("Superb!"));
			return;
		}
		
		if (health < -80) {
			GameController.Instance.VoiceManager.PlayAwesome();
			StartCoroutine(voiceText("Awesome!"));
			return;
		}
	}
	
	private IEnumerator finish() {
		GameController.Instance.GuiManager.VoiceText.text = "Finish It!";
		GameController.Instance.VoiceManager.PlayFinish();
		yield return new WaitForSeconds(2.0f);
		GameController.Instance.GuiManager.VoiceText.text = "";
	}
	
	private IEnumerator voiceText(string text) {
		GameController.Instance.GuiManager.VoiceText.text = text;
		yield return new WaitForSeconds(2.0f);
		GameController.Instance.GuiManager.VoiceText.text = "";
	}
	
	public void AddPower(int player, int power) {
		
		if (!IsActive) return;
		if (player == 1) {
			PlayerOne.Power += power;
		} else {
			PlayerTwo.Power += power;
		}
	}
	
	public void StartGame() {
		p1Finish = false;
		p2Finish = false;
	}
	
	void OnEnable() {
		GameController.Instance.ScoreManager = this;
		PlayerOne = new PlayerStats();
		PlayerTwo = new PlayerStats();
	}
}
