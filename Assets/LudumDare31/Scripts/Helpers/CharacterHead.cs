﻿using UnityEngine;
using System.Collections;

public class CharacterHead : MonoBehaviour {

	public float		speed;

	private int			enemy;
	private int			player;
	private Character	character;
	
	private bool		blood;

	void OnCollisionEnter2D(Collision2D collision) {
	
		D.Trace("[CharacterHead] OnCollisionEnter");
		D.Log("collided with {0} {1}", collision.gameObject.tag, collision.gameObject);
		
		if (character == null)
			return;
			
		if (collision.gameObject.tag == character.Enemy) {
		
			float speed = collision.relativeVelocity.magnitude;
			D.Detail("head speed {0}", speed);
			
			if (character.GetController().GetState() != CharacterStates.ATTACKING)
				return;
			
			if (speed < 10)
				return;
			
			if (!blood) {
				Transform location = collision.transform;
				GameObject e = GameController.Instance.EffectManager.MediumBloodEffect();
				e.transform.parent = location;
				e.transform.localPosition = new Vector2(0,0);
				StartCoroutine(bloodDelay());
			}
			
			GameController.Instance.ScoreManager.AddHealth(enemy, -(50+character.Strength+(int)speed));
			GameController.Instance.ScoreManager.AddScore(player, 500);
			GameController.Instance.ScoreManager.AddExperience(player, 250);
			GameController.Instance.ScoreManager.AddPower(player, 125);
			
			GameController.Instance.SoundManager.PlayHit();
			
			
			if (speed > 50) {
				GameController.Instance.VoiceManager.PlayRamming();
				collision.gameObject.rigidbody2D.AddForce(new Vector2(0,character.Strength*2), ForceMode2D.Impulse);
			}
		}
	}
	
	public void StartGame() {
		character = transform.parent.GetComponent<Character>();
		enemy = 1;
		player = 2;
		if (character.Enemy == "Player2") {
			player = 1;
			enemy = 2;
		}
	}
	
	private IEnumerator bloodDelay() {
		blood = true;
		yield return new WaitForSeconds(1.0f);
		blood = false;
		yield return null;
	}
	
	void OnEnable() {
		D.Trace("[CharacterHead] OnEnable");
	}
	
}
