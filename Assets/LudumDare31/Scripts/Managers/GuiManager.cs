﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GuiManager : MonoBehaviour {

	public GameObject				PlayerPanel;
	public GameObject				GamePanel;
	public GameObject				ChoosePanel;
	public GameObject				RoundoverPanel;
	public PlayerOne				PlayerOneGui;
	public PlayerTwo				PlayerTwoGui;
	public UnityEngine.UI.Text		CountdownText;
	public UnityEngine.UI.Text		VoiceText;
	public GameObject				MessagePrefab;
	public GameObject				DifficultyPanel;
	public UnityEngine.UI.Button	EasyButton;
	public UnityEngine.UI.Button	MedButton;
	public UnityEngine.UI.Button	HardButton;
	public UnityEngine.UI.Button	InsaneButton;
	
	public void ShowDifficulty() {
		DifficultyPanel.transform.localPosition = new Vector2(0,0);		
	}
	
	public void SetEasy() {
		resetButtons();
		EasyButton.GetComponent<Image>().color = Color.green;
		GameController.Instance.GameManager.SetP2("DumbestCharacterController");
	}
	
	public void SetMeduim() {
		resetButtons();
		MedButton.GetComponent<Image>().color = Color.green;
		GameController.Instance.GameManager.SetP2("DumberCharacterController");
	}
	
	public void SetHard() {
		resetButtons();
		HardButton.GetComponent<Image>().color = Color.green;
		GameController.Instance.GameManager.SetP2("DumbCharacterController");
	}
	
	public void SetInsane() {
		resetButtons();
		InsaneButton.GetComponent<Image>().color = Color.green;
		GameController.Instance.GameManager.SetP2("SmartCharacterController");
	}
	
	private void resetButtons() {
		EasyButton.GetComponent<Image>().color = Color.white;
		MedButton.GetComponent<Image>().color = Color.white;
		HardButton.GetComponent<Image>().color = Color.white;
		InsaneButton.GetComponent<Image>().color = Color.white;
	}

	public void PlayAgain() {
		GameController.Instance.GameManager.ResetGame();
		showPanel(PlayerPanel);
	}
	
	public void InitOnePlayer() {
		showPanel(ChoosePanel);
		GameController.Instance.GameManager.InitGame1();
	}
	
	public void InitTwoPlayer() {
		showPanel(ChoosePanel);
		GameController.Instance.GameManager.InitGame2();
	}
	
	public void StartGame() {
		showPanel(GamePanel);
		GameController.Instance.GameManager.StartGame();
		PlayerOneGui.StartGame();
		PlayerTwoGui.StartGame();
	}
	
	public void RoundOver() {
		showPanel(RoundoverPanel);
	}
	
	void Start() {
		showPanel(PlayerPanel);
	}
	
	private void hideAllPanels() {
		hidePanel(PlayerPanel);
		hidePanel(GamePanel);
		hidePanel(ChoosePanel);
		hidePanel(RoundoverPanel);
	}
	
	private void hidePanel(GameObject panel) {
		panel.transform.position = new Vector2(0,-5000);
	}
	
	private void showPanel(GameObject panel) {
		hideAllPanels();
		panel.transform.position = new Vector2(0,0);
	}
	
	void OnEnable() {
		GameController.Instance.GuiManager = this;
		DifficultyPanel.transform.localPosition = new Vector2(0,-1000);		
	}
	
}
